<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage=""  
import="com.cht.bms.eqw2.eqw22003.Util.TimeUtils,
		 com.cht.bms.eqw2.eqw22003.dao.EmployeeDAO,	
		 org.slf4j.Logger,
		 org.slf4j.LoggerFactory"
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>行動上網計時卡明細查詢作業</title>
<%
final Logger logger = LoggerFactory.getLogger("index.jsp");
 //---------------------------initial---------------------------------

/*
String empID = (String)session.getAttribute("empID");
String RemoteIP = (String)session.getAttribute("RemoteIP");
String areaID = (String)session.getAttribute("areaID");
*/
//String empID = "";
//if (System.getProperty("file.separator").equals("\\")) //本機
//{
//	empID  = "816720";
//}
//else
//{
//	empID  = com.cht.sys.CommonUtil.getUserID(request);
//}
String empID  = com.cht.sys.CommonUtil.getUserID(request);
String RemoteIP   = com.cht.sys.CommonUtil.getRemoteIP(request);
String areaID=com.cht.sys.CommonUtil.getOfficecode(empID);
String sbillym = Integer.toString(TimeUtils.getRCYear(TimeUtils.getCurrentYear())) + Integer.toString(TimeUtils.getCurrentMonth()); 

logger.info("--------------行動上網計時卡明細查詢作業 eqw22003------------------");	
logger.info("empID==" + empID);	
logger.info("RemoteIP==" + RemoteIP);	
logger.info("areaID==" + areaID);


int  limitym = 7; //  限制查詢六個月 for employee_class < 5
if(empID != null && !empID.equals("")){
	EmployeeDAO empdao = new EmployeeDAO();
	String employee_class = empdao.getPermission(empID);
	logger.info("employee_class==" + employee_class);	
	if(employee_class!=null && employee_class.compareTo("5") >= 0)
		limitym = 12;
}
else{
	empID="816720";
	RemoteIP="10.144.91.52";
	areaID="223";
}

//------------------------------------------------------------------------
%>
<script type="text/javascript" src="static/js/timeUtils.js"></script>
<script type="text/javascript" src="static/js/windowUtils.js"></script>
<!-- <script type="text/javascript" src="static/js/verifyForm.js?20181203"></script> -->
<script>
String.prototype.trim = function () {
	  return this.replace(/^\s+|\s+$/g, "");
	}
	 
	function checkEQForm(form1){
		var telnum=form1.telnum.value.trim();
		var acctno=form1.acctno.value.trim();
	   if(form1.sbillym)
	   	var sbillym=form1.sbillym.value.trim();
	   else
			var sbillym='';	
		if(form1.s_year)
			var s_year=form1.s_year.value.trim();
		else
			var s_year='';
		if(form1.ebillym)
			var ebillym=form1.ebillym.value.trim();
		else
			var ebillym='';
		if(form1.e_year)	
			var e_year=form1.e_year.value.trim();
		else
			var e_year='';
		
		telformat = /^[0][9]\d{8}/;
		billymformat = /\d{4}/;
		   
	   if(telnum!='' ||acctno!=''){
		   if (telnum!='' && !telformat.test(telnum)){
			   alert("您輸入的電話號碼格式有誤");
			   return false;
			}
			acctnoformat = /^[a-zA-Z0-9]{11}/;
			if (acctno!='' && !acctnoformat.test(acctno)){
			   alert("您輸入的用戶帳號格式有誤");
			   return false;
			}
	   }else{
	      alert("請您輸入電話號碼或用戶帳號");
	   	return false;
	   }
	   
	   if(sbillym=='' && s_year==''){
	      alert("請您輸入起始出帳年月或日期");
	   	return false;
	   }
	   else if (sbillym && !billymformat.test(sbillym)){
	      alert("您輸入起始出帳年月格式有誤");
	   	return false;
	   }
	   
	   if(ebillym=='' && e_year==''){
	      alert("請您輸入終止出帳年月或日期");
	   	return false;
	   }
	   else if (ebillym  && !billymformat.test(ebillym)){
	      alert("您輸入終止出帳年月格式有誤");
	   	return false;
	   }
	   else if (ebillym  < sbillym){
	      alert("您輸入的出帳年月範圍有誤");
	   	return false;
	   }
	   else if (e_year){
	     var sDT = makeDate(parseInt(form1.s_year.value)+1911,form1.s_month.value,form1.s_day.value,form1.s_hour.value,form1.s_min.value,0);
	     var eDT = makeDate(parseInt(form1.e_year.value)+1911,form1.e_month.value,form1.e_day.value,form1.e_hour.value,form1.e_min.value,0);
	     if(sDT.dateDiff("n",eDT) < 0){
	     	  alert("您輸入的起迄時間範圍有誤");
	   	  return false;
	     }	
	   }
	   
	   form1.submit();
	}

	function IsNumber(string,sign) 
	{ 
		var number; 
		if (string==null) return false; 
		if ((sign!=null) && (sign!='-') && (sign!='+'))	{ 
			alert("IsNumber(string,sign)的參數出錯： sign為null或'-'或'+'"); 
			return false; 
		} 
		number = new Number(string); 
		if (isNaN(number)){ 
			return false; 
		} 
		else if ((sign==null) || (sign=='-' && number<0) || (sign=='+' && number>0)) 	{ 
			return true; 
		} 
		else 
			return false; 
	} 

	function makeDate(y,m,d,h,n,s){
	    tmpDate = new Date(y,m,d,h,n,s);
	    return tmpDate;
	}

	Date.prototype.dateDiff = function(interval,objDate){
	    //若參數不足或 objDate 不是日期物件則回傳 undefined
	    if(arguments.length<2||objDate.constructor!=Date) return undefined;
	    switch (interval) {
	      //計算秒差
	      case "s":return parseInt((objDate-this)/1000);
	      //計算分差
	      case "n":return parseInt((objDate-this)/60000);
	      //計算時差
	      case "h":return parseInt((objDate-this)/3600000);
	      //計算日差
	      case "d":return parseInt((objDate-this)/86400000);
	      //計算週差
	      case "w":return parseInt((objDate-this)/(86400000*7));
	      //計算月差
	      case "m":return (objDate.getMonth()+1)+((objDate.getFullYear()-this.getFullYear())*12)-(this.getMonth()+1);
	      //計算年差
	      case "y":return objDate.getFullYear()-this.getFullYear();
	      //輸入有誤
	      default:return undefined;
	    }
	}
</script>
<link href="static/css/BMS.css" rel="stylesheet" type="text/css">
</head>
<body background="static/images/MBMS_background.png" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if (com.cht.sys.CommonUtil.isUserInRole(empID,"BL","99")){%>
<form name="form1" method="post" action="getCustInfo">
<br>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="131"><FIELDSET>
			<div align="center"><font class="title" ><strong>行動上網計時型明細查詢</strong></font></font>
				<input name="OPERATION" type="hidden" id="OPERATION" value="EQ2203">
				<input name="empID" type="hidden" id="empID" value="<%=empID%>">
				<input name="clientIP" type="hidden" id="clientIP" value="<%=RemoteIP%>">
				<input name="areaID" type="hidden" id="areaID" value="<%=areaID%>">
			</div>
			<table width="100%" border="1" align="center" cellpadding="0" cellspacing="1" bordercolor="#DDFBFF" style="BORDER-COLLAPSE: collapse">
				<tr valign="middle" bordercolor="#DDFBFF">
          <td width="98" height="30" class="tr">&nbsp;電話號碼</td>
          <td width="255" height="30" class="content">&nbsp;
            <input name="telnum" type="text" id="telnum" size="15" maxlength="10"></td>
          <td width="98" height="30" class="tr">&nbsp;帳　　號</td>
          <td width="169" height="30" class="content">&nbsp;
            <input name="acctno" type="text" id="acctno2" size="15" maxlength="11"></td>
				</tr>			
				<tr valign="middle" bordercolor="#DDFBFF">
          <td height="30" class="tr">&nbsp;起迄時間<br></td>
          <td height="30" colspan="3" class="content">
						<div id="demployee" style="display=''">&nbsp;
							<select name="s_year" onChange="chgMonth(this.form.s_year, this.form.s_month, this.form.s_day)">
								<script>
									if( <%=TimeUtils.getCurrentMonth()%> >= <%=limitym%>){ cnt= 0;}
									else{cnt = 1 + <%=((limitym - TimeUtils.getCurrentMonth() - 1 ) / 12)%> }
									for(var i = 0 ;  i <= cnt ;  i++ ){
										document.write("<option value='" + reduceTime('y', i , <%= TimeUtils.getCurrentYear()%>) +"'> "  + reduceTime('y', i , <%= TimeUtils.getCurrentYear()%>) + "</option>");
									}
								</script>
							</select>年
							<select name="s_month" onChange="chgMonth(this.form.s_year, this.form.s_month, this.form.s_day)">
								<script>
									var cnt =  (<%=limitym%> >= 12) ? 12 : <%=limitym%>;
									for(i=0; i < cnt ; i++){
										document.write("<option value='" + reduceTime('m', i , <%= TimeUtils.getCurrentMonth()%> )+"'>"+reduceTime('m', i , <%= TimeUtils.getCurrentMonth()%> )+"</option>");
									}
								</script>	
							</select>月
							<select name="s_day">
								<script>
									for( var i=1 ; i <= monthDayMap(<%= TimeUtils.getCurrentYear()%>,<%= TimeUtils.getCurrentMonth()%>) ; i++){
										document.write("<option value='"+ i +"'>"+ i +"</option>");
									}
								</script>
							</select>日
							<select name="s_hour">
              	<script>
									for (i=0;i<=23;i++){
										document.write("<option value='"+ i +"'>"+ i +"</option>");
									}
								</script>
							</select>時
							<select name="s_min">
								<script>
									for (var i=0 ; i <= 59 ;i++ ){
										document.write("<option value='"+ i +"'>"+ i +"</option>");
									}
								</script>
							</select>分　∼
						</div>
					</td>
				</tr>			
				<tr valign="middle" bordercolor="#DDFBFF">
					<td height="30" class="tr">&nbsp; </td>
					<td height="30" colspan="3" class="content">
						<div id="demployee2" style="display=''">&nbsp;
							<select name="e_year" onChange="chgMonth(this.form.e_year, this.form.e_month, this.form.e_day)"> 
								<script>
									if( <%=TimeUtils.getCurrentMonth()%> >= <%=limitym%>){cnt= 0;}
									else{cnt = 1 + <%=((limitym - TimeUtils.getCurrentMonth() - 1 ) / 12)%>}
									for(var i = 0 ;  i <= cnt ;  i++ ){
										document.write("<option value='"+reduceTime('y', i , <%= TimeUtils.getCurrentYear()%>) +"'> "  + reduceTime('y', i , <%= TimeUtils.getCurrentYear()%>) + "</option>");
									}
								</script>
							</select>年
							<select name="e_month" onChange="chgMonth(this.form.e_year, this.form.e_month, this.form.e_day)">
								<script>
									var cnt =  (<%=limitym%> >= 12) ? 12 : <%=limitym%>;
									for(i=0; i < cnt ; i++){
										document.write("<option value='"+reduceTime('m', i , <%= TimeUtils.getCurrentMonth()%> )+"'>"+reduceTime('m', i , <%= TimeUtils.getCurrentMonth()%> )+"</option>");
									}
								</script>
							</select>月
							<select name="e_day">
								<script>
									for( var i=1 ; i <= monthDayMap(<%= TimeUtils.getCurrentYear()%>,<%= TimeUtils.getCurrentMonth()%>) ; i++){
										document.write("<option value='"+ i +"'>"+ i +"</option>");
									}
								</script>
							</select>日
							<select name="e_hour">
								<option value="23" selected>23</option>
								<script>
									for (i=0;i<=23;i++){
										document.write("<option value='"+ i +"'>"+ i +"</option>");
									}
								</script>
							</select>時
							<select name="e_min">
								<option value="59" selected>59</option>
								<script>
									for (var i=0 ; i <= 59 ; i++){
										document.write("<option value='"+ i +"'>"+i+"</option>");
									}
								</script>
							</select>分
						</div>
					</td>
				</tr>
				<!-- 2013.09 行資41-102-0328-驗41-01需求增加excel報表格式 -->
				<tr valign="middle" bordercolor="#DDFBFF">
					<td height="30" class="tr">&nbsp;檔案格式</td>
					<td height="30" class="content" colspan=3>&nbsp;
						<select name="reporttype">
							<option value="PDF" selected>PDF
							<option value="EXCEL">EXCEL
						</select>
					</td>         
				</tr>
			</table>
    </FIELDSET>
    </td>
  </tr>
</table>
<div align="center"><br>
    <input  type="button" value="查詢" class="but-01" onclick="checkEQForm(this.form)">
&nbsp;&nbsp; &nbsp;&nbsp;
  <input name="重設"  type="reset" class="but-01" value="清除">
  
</div>
</form>
<%}else{
		//out.println( "<p><div align='center' class='title-02'>網頁過期，請重新開啟本頁籤！</div></p>");
		out.println( "<p><div align='center' class='title-02'>您未被授權執行此功能!</div></p>");
}%>
</body>

<script type="text/javascript">
	document.Charset = "big5";
</script>

</html>
