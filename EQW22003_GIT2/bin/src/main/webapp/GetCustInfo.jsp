<%@ page contentType="text/html; charset=big5"  
import="javax.naming.*,
				javax.sql.*,
				java.util.*,
				java.text.SimpleDateFormat,
				com.cht.bms.eqw2.eqw22003.JavaBean.CusInfo,
				com.cht.bms.eqw2.eqw22003.JavaBean.UserInput,
				com.cht.bms.eqw2.eqw22003.Util.TimeUtils,
				com.cht.sys.CommonUtil,
				com.cht.bms.eqw2.eqw22003.Util.FormatUtil,
				org.slf4j.Logger,
				org.slf4j.LoggerFactory"
				%>
<!--query customer information-->	
<%
//tester svn again test
	request.setCharacterEncoding("big5");
	
	final Logger logger = LoggerFactory.getLogger("GetCustInfo.jsp");

	CusInfo cusinfo = null;
	Boolean decode = null;
	String hide = null;

    //取得欲查詢的客戶資料
	String region = null;
    UserInput usr_input = null;
    String ispostback = request.getParameter("ispostback");

    if(ispostback!=null && ispostback.equals("true"))//若頁面已重送(按下解碼)
    {
    	usr_input = new UserInput();
		usr_input.setUsrid(request.getParameter("empID"));		
		usr_input.setClientIP(request.getParameter("clientIP"));		
		usr_input.setAreaID(request.getParameter("areaID"));
    	usr_input.setSdate(request.getParameter("startdate"));
    	usr_input.setEdate(request.getParameter("enddate"));
		//2013.09 行資41-102-0328-驗41-01需求增加excel報表格式
    	usr_input.setReporttype(request.getParameter("reporttype"));
    } else {
		request.getSession().removeAttribute("hide");
		request.getSession().removeAttribute("decode");
		String usrid = null;
		String clientIP = null;		
		String areaID = null;
	
		String telnum = null;
		String acctno = null;
		
		String s_year = null;
		String s_month = null;
		String s_day = null;
		String s_hour = null;
		String s_min = null;	
			
		String e_year = null;
		String e_month = null;
		String e_day = null;
		String e_hour = null;
		String e_min = null;

	}

	if(ispostback==null || ispostback.length()==0) {

		cusinfo = (CusInfo)request.getAttribute("cusinfo");	
		usr_input = (UserInput)request.getAttribute("usr_input");	
		session.setAttribute("cusinfo",cusinfo);		

	}
	//網頁已post過，cusinfo直接由session中取得
	else {
		cusinfo = (CusInfo) session.getAttribute("cusinfo");
		if(cusinfo==null) out.println("get customer info 失敗");
	}

	try {
		//decode用來判斷是否已解碼成功
		decode = (Boolean) request.getAttribute("decode");
		if(decode == null)
			decode = new Boolean(false);  
		String idcode = request.getParameter("idcode");  //輸入的證號後四碼內容
		//判斷證號後四碼是否相同，用decode變數記錄
		if(!(idcode == null || idcode.length()==0) && cusinfo!=null)  
		{
			String cust_id = "";
			if( cusinfo.getCuDB().getBosspid() != null)
				cust_id = cusinfo.getCuDB().getBosspid();
			if( cusinfo.getCuDB().getPersonid() != null)
				cust_id = cusinfo.getCuDB().getPersonid();
			if(cust_id.length()>=4)
			{
				if(cust_id.substring(cust_id.length()-4,cust_id.length()).equals(idcode))
				{
					decode = new Boolean(true);
				}
				//out.println("cust_id = "+cust_id);
				//out.println("cust_id.substring(cust_id.length()-4,cust_id.length()) = "+cust_id.substring(cust_id.length()-4,cust_id.length()));
			}
		}
		session.setAttribute("decode",decode);
		
		//dutycode=B1~B3，則hide=Depend；否則hide=Decode
		hide = (String)session.getAttribute("hide");
		if(hide==null || hide.length()==0)  //進行hide初始化
		{
		    //String employeeno = CommonUtil.getUserID(request);
		    String employeeno = com.cht.sys.CommonUtil.getUserID(request);
			List<String> duty = com.cht.sys.CommonUtil.getDutyList(employeeno);
			//dutycode in roleMap，則解碼；否則需輸入後證號四碼
			Map<String, String> roleMap = new HashMap<String, String>();
		   	//List<String> roleList = new ArrayList<String>();
		   	List<String> roleList = null;
		   	if(request.getAttribute("roleList")!=null){
		   		roleList = (List<String>)request.getAttribute("roleList");
		   	}
			if(roleList != null) {
				for(String role : roleList) {
					roleMap.put(role, String.valueOf("1"));
				}
			}
		    hide = "Depend";//隱碼,但可解碼
		    for (String s : (List<String>) duty) {
		        if(roleMap.get(s) != null)  {
		        	hide="Decode";//不隱碥
		        	break;
		        }
		    }
		    request.getSession().setAttribute("hide",hide);
	    }
	} catch (Exception ex) {
		out.println("判斷隱碼 失敗");
		ex.printStackTrace();
		return;
	}

%>		
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>客戶資料查核</title>
<link href="static/css/BMS.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="static/js/windowUtils.js"></script>
</head>
<body background="static/images/MBMS_background.png">
<%if(!decode.booleanValue()&&!hide.equals("Decode")){%>
<div align="center">
	<form name="decode" method="post" action="GetCustInfo.jsp">
		請輸入客戶證號後四碼：<input name="idcode" type="text" id="idcode" size="10" maxlength="10"/>
	  		<input type="hidden" name="custname" value="<%=cusinfo.getCuDB().getCustname()==null?"":cusinfo.getCuDB().getCustname()%>">
			<input type="hidden" name="telnum" value="<%=cusinfo.getTelnum()%>">
			<input type="hidden" name="accountno" value="<%=cusinfo.getAccountno()%>">
			<input type="hidden" name="empID" value="<%=usr_input.getUsrid()%>">	
			<input type="hidden" name="clientIP" value="<%=usr_input.getClientIP()%>">	
			<input type="hidden" name="areaID" value="<%=usr_input.getAreaID()%>">	
			<input type="hidden" name="startdate" value="<%=usr_input.getSdate()%>">	
			<input type="hidden" name="enddate" value="<%=usr_input.getEdate()%>">	
			<!-- 2013.09 行資41-102-0328-驗41-01需求增加excel報表格式 -->
			<input type="hidden" name="reporttype" value="<%=usr_input.getReporttype()%>">	
			<input type="hidden" name="ispostback" value="true">
		<input type="submit" name="Submit" class="but-02" value="解碼"/>
	</form>
</div>
<%}%>
<p>&nbsp;</p>
<div align="center">
<form name="myform" method="Get" action="LoadCDRService">
<%if( cusinfo != null && (cusinfo.getErrMsg() == null || cusinfo.getErrMsg().equals("") )){%>
<table width="500" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#FF6600">
  <tr>
	  	<td>
	  		<input type="hidden" name="custname" value="<%=cusinfo.getCuDB().getCustname()==null?"":cusinfo.getCuDB().getCustname()%>">
			<input type="hidden" name="telnum" value="<%=cusinfo.getTelnum()%>">
			<input type="hidden" name="accountno" value="<%=cusinfo.getAccountno()%>">
			<input type="hidden" name="empID" value="<%=usr_input.getUsrid()%>">	
			<input type="hidden" name="clientIP" value="<%=usr_input.getClientIP()%>">	
			<input type="hidden" name="areaID" value="<%=usr_input.getAreaID()%>">	
			<input type="hidden" name="startdate" value="<%=usr_input.getSdate()%>">	
			<input type="hidden" name="enddate" value="<%=usr_input.getEdate()%>">	
			<!-- 2013.09 行資41-102-0328-驗41-01需求增加excel報表格式 -->
			<input type="hidden" name="reporttype" value="<%=usr_input.getReporttype()%>">	
			<input type="hidden" name="ispostback" value="true">
		</td>
	</tr>
	<tr>
    <td bordercolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td height="30" >電　　話：<%=cusinfo.getTelnum()%></td>
        <td height="30" >帳　　號：<%=cusinfo.getAccountno()%></td>
      </tr>
       <tr>
        <td height="150" colspan="2" ><p>客戶名稱：
        <%
        	String custName = null;
        	if(cusinfo.getCuDB() != null && cusinfo.getCuDB().getCustname() != null) 
        		custName = cusinfo.getCuDB().getCustname();
            if (custName == null) 
            {
            	custName="";
            }
            else if(custName.length()>0)
            {
                if (hide.equals("Decode"))// 解碼
                    ;
                else if (hide.equals("Depend")) // 選擇性
                {
                    if (!decode.booleanValue()) 
                    {
                        custName = FormatUtil.hiddenCode(custName, 'M');
                    }
                } 
                else // 隱碼
                    custName = FormatUtil.hiddenCode(custName, 'M');
            }
        	
        %>
          <%=custName %>
          </p>
          <p>地　　址：(<%=cusinfo.getMcDB().getBillzipcode()%>)
          <%
          	String address = cusinfo.getAddress();
          	if(address == null || address.length()==0)
          	{
          		if(cusinfo.getCuDB() != null && cusinfo.getCuDB().getCustaddr() != null) {
          			address = cusinfo.getCuDB().getCustaddr();
          		}
	          	if(address == null || address.length()==0) {
	          		if(cusinfo.getMcDB() != null && cusinfo.getMcDB().getBilladdr() != null)
	          			address = cusinfo.getMcDB().getBilladdr();
	          	}
          	}
            if (address == null) 
            {
            	address = "";
            }
            else if(address.length()> 0)
            {
                if (hide.equals("Decode"))// 解碼
                    ;
                else if (hide.equals("Depend")) // 選擇性
                {
                    if (!decode.booleanValue()) 
                    {
                        address = FormatUtil.hiddenCode(address, 'A');
                    }
                } 
                else // 隱碼
                    address = FormatUtil.hiddenCode(address, 'A');
            }
          %>
          <%=address %>
          </p>
          <p><font color="#FF6600">用戶基本資料如下：</font></p>
          <table width="520" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td>                <table width="100%" border="1" align="center" cellpadding="0" cellspacing="1" bordercolor="#DDFBFF">
                <tr valign="middle" bordercolor="#DDFBFF" class="tr-02">
                  <td width="31%" height="25">
                    <div align="center"><font color="#32659C">轉帳代繳帳號</font></div>
                  </td>
                  <td width="28%" height="25">
                    <div align="center"><font color="#32659C">身份証號</font></div>
                  </td>
                  <td width="24%" height="25"><div align="center"><font color="#32659C">包月制生效年月</font></div>
                  </td>
                  <td width="17%" height="25"><div align="center"><font color="#32659C">是否限閱</font></div>
                  </td>
                </tr>
                <tr bordercolor="#DDFBFF" bgcolor="#DDFBFF">
                  <td height="25">
                    <div align="center">
                      <%if (cusinfo.getMcDB().getBankacctno()!=null) out.print(cusinfo.getMcDB().getBankacctno());%>
                    </div>
                  </td>
					<%
					String id = "";
						if( cusinfo.getCuDB().getBosspid() != null)
							id = cusinfo.getCuDB().getBosspid();
						if( cusinfo.getCuDB().getPersonid() != null)
    						id = cusinfo.getCuDB().getPersonid();
	   				%>
                  <td height="25">
                    <div align="center">
			        <%
			            if (id == null) 
			            {
			            	id="";
			            }
			            else if(id.length() > 0)
			            {
			                if (hide.equals("Decode"))// 解碼
			                    ;
			                else if (hide.equals("Depend")) // 選擇性
			                {
			                    if (!decode.booleanValue()) 
			                    {
			                        id = FormatUtil.hiddenCode(id, 'I');
			                    }
			                } 
			                else // 隱碼
			                    id = FormatUtil.hiddenCode(id, 'I');
			            }
			        	
			        %>
			        <%=id %>
                    </div>
                  </td>
                  <td height="25"><div align="center">
										<%if(cusinfo.getBDeffectDate()!=null) out.print(cusinfo.getBDeffectDate());%>
									</div></td>
                  <td height="25"><div align="center">
										<%if(cusinfo.getLrb()!= null) out.print(cusinfo.getLrb());%>
                  </div></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center">
					<font color="#FF0000">
		      <%
		      	if(cusinfo.getLrb()!= null ){
							if (cusinfo.getLrb().equals("y") || cusinfo.getLrb().equals("Y"))
			     			out.print("請注意!此客戶有限閱全部通話明細");
							else if (cusinfo.getLrb().equals("a") || cusinfo.getLrb().equals("A"))
			     			out.print("請注意!此客戶有限閱國內及PRS通話明細");
		   				else if (cusinfo.getLrb().equals("b") || cusinfo.getLrb().equals("B"))
			     			out.print("請注意!此客戶有限閱國際及國際漫遊通話明細");
       			}
       		%>
       		<% 
       			if(cusinfo.getSelf_flag()==1)
	  		   		out.println("請注意! 該客戶有申請限本人辦理事項");
   	   		%>
	  			<% 
	  				if(cusinfo.getC5_flag()==1)
							out.println("(1)新帳號"+cusinfo.getMcDB().getAccountno()+"(2)舊帳號 "+cusinfo.getOldaccountno());
        	%>
          </font>
				</div></td>
      </tr>
    </table></td>
  </tr>
</table>
<p>
<%
}else if(cusinfo==null){
	out.println( "<p><div class='title-02'>cusinfo為null!</div></p>");
}else{
	out.println( "<p><div class='title-02'>"+cusinfo.getErrMsg() +"</div></p>");
}
%>
</p>
<p align="center">  
<% if(cusinfo.getErrMsg() == null  ){%>
<input type="submit" name="Submit"  class="but-02" value="確認查詢">
<%}%>
<input type="button" name="B1" class="but-02" value="回上一頁"  onClick="javascript:history.back()">
</p>
</form>
</body>

<script type="text/javascript">
	document.Charset = "big5";
</script>

</html>